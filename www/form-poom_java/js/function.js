function validate(form)
{
	fail = validateFirstName(form.firstname.value);
	fail += validateLastName(form.lastname.value);
	fail += validateUserName(form.username.value);
	fail += validatePassword(form.password.value);
	fail += validateAge(form.age.value);
	fail += validateEmail(form.email.value);
 
	if(fail == "")
	{
		//fail = "Congraats"

		return true;
	}
	else 
	{
		//alert(fail);
		document.getElementById('insertHere').innerHTML = fail;
		//return false;
	}
}

function validateFirstName(field)
{
	if(field == "")
	{
		return "<li>Name must not be blank</li>";
	}
	else if(/\s/.test(field))
	{
		return "<li>Name must not contain spaces</li>";
	}
	else if(field.length < 3 && field.length >= 1)
	{
		return "<li>Name must be at least 3 characters</li>";
	}
	else if(/[(@!#\$%\^\&*\)\(+=._-]/.test(field))
	{
		return "<li>Name not contain special symbol</li>"
	}
	else if(/[0-9]/.test(field))
	{
		return "<li>Name not contain number</li>>"
	}
	return "";
}

function validateLastName(field)
{
	if(field == "")
	{
		return "<li>Lastname must not be blank</li>";
	}
	else if(/\s/.test(field))
	{
		return "<li>Lastname must not contain spaces</li>";
	}
	else if(field.length < 3 && field.length >= 1)
	{
		return "<li>Lastname must be at least 3 characters</li>";
	}
	else if(/^[^a-zA-Z0-9]+$/.test(field))
	{
		return "<li>Lastname not contain special symbol and number</li>"
	}
	return "";
}

function validateUserName(field)
{
	if(field == "")
	{
		return "<li>UserName must not be blank</li>";
	}
	else if(field.length < 5)
	{
		return "<li>User must be at least 5 characters</li>";
	}
	return "";
}

function validatePassword(field)
{
	if(field == "")
	{
		return "<li>Password must not be blank</li>";
	}
	else if(field.length < 8)
	{
		return "<li>Password must be at least 8 characters</li>";
	}
	else if(!/[0-9]/.test(field))
	{
		return "<li>Password must contain digit</li>"
	}
	else if(!/[(@!#\$%\^\&*\)\(+=._-]/.test(field))
	{
		return "<li>Password must contain special</li>"
	}
	return "";
}

function validateAge(field)
{
	if(field == "")
	{
		return "<li>Age must not be blank</li>";
	}
	else if(field <18 || field >110)
	{
		return "<li>The age must be between 18 and 110</li>";
	}
	return "";
}

function validateEmail(field)
{
	if(field == "")
	{
		return "<li>Email must not be blank</li>";
	}
	else if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(field))
	{
		return "<li>Incorrect email format</li>";
	}
	return "";
}

function checkName() {
   var name = $("#txtNewName").val();
   
    if (/[0-9]/.test(name))
    {
        $("#divCheckName").html("Name must not contain number");
    }
    else if(/\s/.test(name))
    {
    	$("#divCheckName").html("Name mut not contanin space");
    }
    else if(/[!@#$%&*()_+=>?{}~-]/.test(name))
    {
    	$("#divCheckName").html("Name mustn't contain symbol and digit");
    }
    else if(name.length < 3 && name.length >= 1)
    {
		$("#divCheckName").html("Name at least 3 char");
    }
    else
        $("#divCheckName").html("");
}

function checkLname() {
   var lname = $("#txtNewLname").val();
   
    if (/[0-9]/.test(lname))
    {
        $("#divCheckLname").html("Lastname must not contain number");
    }
    else if(/\s/.test(lname))
    {
    	$("#divCheckLname").html("Lastname mut not contanin space");
    }
    else if(/[!@#$%&*()_+=>?{}~-]/.test(lname))
    {
    	$("#divCheckLname").html("Lastname mustn't contain symbol and digit");
    }
    else if(lname.length < 3 && lname.length >= 1)
    {
		$("#divCheckLname").html("Lastname at least 3 char");
    }
    else
        $("#divCheckLname").html("");
}


function checkUname() {
   var uname = $("#txtNewUname").val();
   
    if (uname.length < 5)
    {
        $("#divCheckUname").html("Username at least 5 char");
    }
    else
        $("#divCheckUname").html("");
}

function checkPass() {
   var pass = $("#txtNewPass").val();
   

    if(pass == "")
    {
    	$("#divCheckPass").html("Password must not be blank");
    }
    else if(!/[(@!#\$%\^\&*\)\(+=._-]/.test(pass))
    {
    	$("#divCheckPass").html("Password must contain special symbol");
    }
    else if(!/[0-9]/.test(pass))
    {
    	$("#divCheckPass").html("Password must contain digit");
    }
    else if (pass.length < 8)
    {
        $("#divCheckPass").html("Username at least 8 char");
    }
    else
        $("#divCheckPass").html("");
}

function checkAge() {
   var age = $("#txtNewAge").val();
   
    if (age < 18 || age > 112)
    {
        $("#divCheckAge").html("age between 18 - 112");
    }
    else
        $("#divCheckAge").html("");
}

function checkEmail() {
   var mail = $("#txtNewMail").val();
   
    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
    {
        $("#divCheckMail").html("Incorrect email format");
    }
    else
        $("#divCheckMail").html("");
}

$("#searchclear").click(function(){
    $("#searchinput").val('');
});