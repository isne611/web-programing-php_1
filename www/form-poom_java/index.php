<!--<html>
<script type="text/JavaScript" src="js/function.js"></script>
<body>

<form    method="post">
Name: <input type="text" name="firstname"><br>
Surname: <input type="text" name="lastname"><br>
Username: <input type="text" name="username"><br>
Password: <input type="password" name="password"><br>
Age: <input type="text" name="age"><br>
Email: <input type="text" name="email"><br>
<input id="clickMe" type="button" value="clickme" onclick="validate(this.form);" />
</form>

</body>
</html> -->

<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/JavaScript" src="js/function.js"></script>
</head>
<style>
    body{
        overflow-x: hidden;
    }
    .main-content
    {
        width: 50%;
        height: 40%;
        margin: 10px auto;
        background-color: #fff;
        border: 2px solid #e6e6e6;
        padding: 40px 50px;
    }
    .header
    {
        border: 0px solid #000;
        margin-bottom: 5px;
    }
    #check
    {
        width: 60%;
        border-radius: 30px;
    }
    .output
    {
    	color: red;
    }
    .registrationFormAlert
    {
    	color: red;
		transition: 0.5s;
    }
    #searchinput {
    width: 200px;
}
#searchclear {
    position: absolute;
    right: 5px;
    top: 0;
    bottom: 0;
    height: 14px;
    margin: auto;
    font-size: 14px;
    cursor: pointer;
    color: #ccc;
}

</style>
<body>
<div class="row">
    <div class="col-sm-12">
        <div class="main-content">
            <div class="header">
                <h3 style="text-align: center;"><strong>Validation Form</strong></h3>
                <hr>
            </div>
            <div class="l-part">
                <form id="selectform" action="" method="post">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <!--<input type="text" class="form-control" placeholder="First Name" name="firstname" required="required">-->
                        <input type="text" class="form-control" placeholder="First Name" id="txtNewName" onkeyup="checkName();" required="required"/>
                    </div><br>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input type="text" class="form-control" placeholder="LastName" id="txtNewLname" onkeyup="checkLname()"; required="required">
                    </div><br>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
                        <input type="text" class="form-control" placeholder="Username" id="txtNewUname" onkeyup="checkUname();"required="required">
                    </div><br>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-asterisk"></i></span>
                        <input type="password" class="form-control" placeholder="Password" id="txtNewPass" onkeyup="checkPass();" required="required">
                    </div><br>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-font"></i></span>
                        <input type="text" class="form-control" placeholder="Age" id="txtNewAge" onkeyup="checkAge();" required="required">
                    </div><br>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                        <input type="text" class="form-control" placeholder="Email" id="txtNewMail" onkeyup="checkEmail();" required="required">
                    </div><br>
                    <center>
                    <button class="btn btn-warning" onclick="document.getElementById('button').reset();">Clear</button>
                    <button type="button" class="btn btn-primary" onclick="validate(this.form);return false;" >Send</button>
                    </center>
                  
             		<div> </div><br>
                    <div class="registrationFormAlert" id="divCheckName"></div>
                    <div class="registrationFormAlert" id="divCheckLname"></div>
                    <div class="registrationFormAlert" id="divCheckUname"></div>
                    <div class="registrationFormAlert" id="divCheckPass"></div>
					<div class="registrationFormAlert" id="divCheckAge"></div>
					<div class="registrationFormAlert" id="divCheckMail"></div>
					<div></div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>