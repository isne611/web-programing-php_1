<html>
<body> 
   <?php 
      $name = $_POST["name"];
      $sname = $_POST["sname"];
      $uname = $_POST["uname"];
      $passw = $_POST["passw"];
      $age = $_POST["age"];
      $email = $_POST["email"]; 
      $expr = '/^[1-9][0-9]*$/';   

      if(strlen($name)<3&&strlen($name)>=1 || preg_match("/\W/", $name) || preg_match("/[0-9]/", $name))
      { 
         echo "Name must be at least 3 characters or not contain special symbol and number.";echo "<br>";
      }
      else if ($name=="") 
      {
        echo "Name must not be blank";echo "<br>";
      }
      else if ($name == trim($name) && strpos($name, ' ') !== false) 
      {
      echo "Name must not contain spaces";echo "<br>";
     }
      else
      {
        echo "Welcome " .$name. " ";
      }
    

    if(strlen($sname)<3&&strlen($sname)>=1 || preg_match("/\W/", $sname) || preg_match("/[0-9]/", $sname))
      { 
         echo "Surname must be at least 3 characters or not contain special symbol and number."; 
      }
      else if ($sname=="") 
      {
        echo "Surname must not be blank";echo "<br>";
      }
      else if ($sname == trim($sname) && strpos($sname, ' ') !== false) 
      {
      echo "Surname must not contain spaces";echo "<br>";
    }
      else
      {
        echo "" .$sname. ".";echo "<br>";
      }

      if(strlen($uname)<5) 
      { 
         echo "User must be at least 5 characters.";
      }
      else
      {
        echo "Username is: " .$uname;echo "<br>";
      }

      if (strlen($passw) < 8) 
      {
       echo "Password must  be  at  least  8  characters";
    }
    else if (!preg_match("/\d/", $passw)) 
    {
      echo "Password should contain at least one digit";
    }
    else if (!preg_match("/[A-Z]/", $passw)) 
    {
        echo "Password should contain at least one Capital Letter";
    }
    else if (!preg_match("/[a-z]/", $passw)) 
    {
        echo "Password should contain at least one small Letter";
      }
      else if (!preg_match("/\W/", $passw)) 
      {
        echo "Password should contain at least one special character";
    }
    else
      echo "Your password is: " .$passw;echo "<br>";

    if($age<"18" && $age>"110")
    {
      echo " The age must be between 18 and 110 ";
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
    {
      echo "Invalid email format";echo "<br>";
    }
    else
    {
      echo "Your e-mail is: " .$email;echo "<br>";
    }


   ?> 
</body>
</html>